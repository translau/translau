import * as fs from 'fs';
import path, { ParsedPath } from 'path';

import * as express from 'express';

import { IData } from './data.interface';
import { IFolder } from './folder.interface';

const supportedExtensions: string[] = ['.json'];

export async function getTranslations(req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> {
    try {
        const directory: string = req.app.get('working-directory');
        res.json(
            traverseFolder(directory)
        );
    } catch (error) {
        next(error);
    }
}

function loadFile(file: string): IData {
    try {
        const rawData = fs.readFileSync(file, 'utf8');
        return JSON.parse(rawData);
    } catch (error) {
        console.error(error);
    }
    return {};
}

function traverseFolder(currentPath: string = ''): IFolder {

    let data: IFolder = {
        name: path.basename(currentPath)
    };

    const files: fs.Dirent[] = fs.readdirSync(currentPath, { withFileTypes: true });

    for (const file of files) {
        if (file.isDirectory()) {
            data.subFolders = data.subFolders || [];
            data.subFolders.push(traverseFolder(path.join(currentPath, file.name)));
        } else if (file.isFile()) {
            const fileInfo: ParsedPath = path.parse(file.name);
            if (!supportedExtensions.includes(fileInfo.ext)) {
                continue;
            }
            data.files = data.files || [];
            data.files.push({
                language: fileInfo.name,
                data: loadFile(`${currentPath}/${file.name}`)
            });
        }
    }

    return data;
}
