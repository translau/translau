import * as fs from 'fs-extra';
import path from 'path';

import * as express from 'express';

import { IFolder } from '../get/folder.interface';

export async function postTranslations(req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> {
    try {
        const directory: string = req.app.get('working-directory');
        await traverseFolder(directory, req.body);
        res.status(201).send();
    } catch (error) {
        next(error);
    }
}

async function traverseFolder(directory: string, folder: IFolder): Promise<void> {

    if (folder.subFolders) {
        for (const subFolder of folder.subFolders) {
            const subFolderPath: string = path.join(directory, subFolder.name);
            await fs.ensureDir(subFolderPath);
            traverseFolder(subFolderPath, subFolder);
        }
    }

    if (folder.files) {
        for (const file of folder.files) {
            const fileName: string = path.join(directory, `${file.language}.json`);
            await fs.ensureFile(fileName);
            await fs.writeJSON(fileName, file.data, { spaces: '\t' });
        }
    }

}
