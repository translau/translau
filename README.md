# Translau

![npm](https://img.shields.io/npm/v/translau.svg)

Translation manager tool with GUI.

![Translau showcase](https://translau.gitlab.io/img/translau.gif)

## Usage

```bash
translau <TRANSLATION_FOLDER>
npx translau <TRANSLATION_FOLDER> # using npx
```

## Features

For full list visit [documentation](https://translau.gitlab.io).

### Implemented

* loading of json translation files from specified folder
* structure explorer
* language manager
* translations editor
* missing translations counters
* saving of edited values to file system

### Planed

* import / export to json/csv
* control git repository through gui
* linked translations (dictionary)
* example of usage in code (aurelia specific)
* options support with CLI interface and config file
* adding value by path
* move/copy values by path (mv & cp like)

## Installation

```bash
npm install
```

## Run dev app

```bash
npm run start
```

## Build

```bash
npm run build
```

## Unit tests

```bash
npm test
```
